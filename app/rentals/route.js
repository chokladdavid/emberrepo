import Ember from 'ember';

export default Ember.Route.extend({
	model() {
		return this.get('store').findAll('rental').catch(error=>console.log(error));
	}
});
