import DS from 'ember-data';

export default DS.Model.extend({
	residentImage: DS.attr('string'),
    residentName: DS.attr('string'),
    ownerName: DS.attr('string'),
    location: DS.attr('string'),
    residentType: DS.attr('string'),
    numberOfBedrooms: DS.attr('number')
});
